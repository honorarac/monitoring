# Monitoring

This repository is mirrored on https://ops.gitlab.net/gitlab-com/gl-infra/k8s-workloads/monitoring

Kubernetes Workload configurations for GitLab.com

## Storage

:warning: **WARNING** :warning:

The following are _NOT_ allowed this repository:
* Files that create Kubernetes Objects of type `Secret`
* Files that contain secrets in plain text

## Installation

### Manual Actions

We need to create a secret for our Thanos object storage configuration.  In
1Password we have a yaml file dedicated for each environment:
`<env>-thanos-secret.yaml`  Pull down this file, name it objstore.yaml, and
perform the following on the correct Kubernetes cluster you intend to work on:

```
kubectl create secret generic thanos-objstore-config --from-file=objstore.yaml --namespace monitoring
```

If this is a fresh cluster where an `install` operation has not been completed,
the namespace may not exist.  Ensure the above step is completed shortly after
the `install` operation is completed for the first time.

### Configuration

Helper scripts are used for adding, removing and updating monitoring in GKE. The
following are required and must be installed locally:

* kubectx
* kubectl
* helm
* jq
* gcloud

All of the helpers will ensure you local environment is setup properly before
running. Note that you must have gcloud installed and configured properly for
cluster you are configuring.

### Managing our Monitoring Services

The `./bin/k-ctl` script is used both locally and in CI to manage the chart for
different environments.

```
    Usage ./bin/k-ctl [-e env] [Dgc] <install|list|remove|template|upgrade|config>

    DESCRIPTION
        This script is wrapper over the helm command for a consistent managing
        of charts and performing maintenance on them

    FLAGS
        -e  environment
        -D  run in Dry-run

    ACTIONS
        install:  Install the helm chart
        list:     List all helm charts that are installed
        upgrade:  Upgrade this chart
        remove:   Remove this chart
        template: Generate kubernetes config using helm template
        config:   Display the configurations for the environment

    EXAMPLES
            ./bin/k-ctl -e pre upgrade
            ./bin/k-ctl -e gstg list
            ./bin/k-ctl -e pre -D upgrade # dry-run mode
            ./bin/k-ctl -e pre config                    # displays variable configurations for environment
```

## Prometheus Operator


Prometheus is configured on GKE using the
[prometheus operator](https://github.com/helm/charts/tree/master/stable/prometheus-operator) helm chart.

Instead of running Tiller on GKE, we currently
run tiller locally to avoid the security and performance implications
of running Tiller on the cluster

* Install helm https://helm.sh/docs/install/
* Install the "tillerless" helm plugin

```
helm plugin install https://github.com/rimusz/helm-tiller
```

* Start a new shell with local helm tiller

```
helm tiller start
# ....
```

* Run a single command with local helm tiller

```
helm tiller run helm list
```

## References

* Kubernetes configuration design https://about.gitlab.com/handbook/engineering/infrastructure/design/kubernetes-configuration/
* Kubernetes workloads configuration for GitLab.com https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com
* tillerless helm https://rimusz.net/tillerless-helm
* prometheus operator https://github.com/helm/charts/tree/master/stable/prometheus-operator
* Helm https://helm.sh/docs/install/
